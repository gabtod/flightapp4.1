package com.example.flightapp41;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetFlightsAsync extends AsyncTask<String, Void, ArrayList<Details>> {
    @Override
    protected ArrayList<Details> doInBackground(String... info) {

        Details d = null;

        ArrayList<Details> details = new ArrayList<>();
        FlightHTTPClient client = new FlightHTTPClient();

        String resultJSON = client.getFlightData(info[0], info[1], info[2], info[3]);

        JSONObject jObj = null;
        try {
            jObj = new JSONObject(resultJSON);

            JSONArray mainObj = jObj.getJSONArray("flightStatuses");

            for(int i = 0; i< mainObj.length(); i++) {
                JSONObject obj = mainObj.getJSONObject(i);

                JSONObject dateObj = obj.getJSONObject("arrivalDate");


                details.add(new Details(obj.getString("flightId"), obj.getString("departureAirportFsCode"),dateObj.getString("dateLocal")));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return details;
}
}
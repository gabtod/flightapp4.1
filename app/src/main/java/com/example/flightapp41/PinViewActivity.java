package com.example.flightapp41;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class PinViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_view);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        if(pinview !=null) {
            pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
                @Override
                public void onDataEntered(Pinview pinview, boolean b) {
                    if(pinview.getValue().equals("1234")) {
                        Toast.makeText(PinViewActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        finish();
                        startActivity(new Intent(getApplicationContext(), WelcomeActivity2.class));
                    }
                    else {
                        Toast.makeText(PinViewActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

}

package com.example.flightapp41;

import java.util.ArrayList;
import java.util.stream.Stream;

public class Details extends ArrayList<Details> {

    private String flightId;
    private String departureAirport;
    private String dateAndTime;



    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureairport) {
        this.departureAirport = departureairport;
    }


    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightid) {
        this.flightId = flightid;
    }


    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String flightNumber) {
        this.dateAndTime = flightNumber;
    }


    public Details(String flightId, String departureAirport, String dateAndTime) {
        this.flightId = flightId;
        this.departureAirport = departureAirport;
        this.dateAndTime = dateAndTime;
    }
}

package com.example.flightapp41;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String SHARED_PREFS = "sharedPrefs";
    private Button btnSignUp;
    private EditText textEmail;
    private EditText textPassword;
    private TextView textLogIn;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        btnSignUp = findViewById(R.id.btnSignUp);
        textEmail = findViewById(R.id.textEmail);
        textPassword = findViewById(R.id.textPassword);
        textLogIn = findViewById(R.id.textLogIn);

        btnSignUp.setOnClickListener(this);
        textLogIn.setOnClickListener(this);


    }

    private void registerUser(){
        String email = textEmail.getText().toString().trim();
        String password = textPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //Email is empty.
            Toast.makeText(this,
                    "Please enter email address.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            //Password is empty.
            Toast.makeText(this,
                    "Please enter password.", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Registering user with data.");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email, password).
                addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //On Success
                            Toast.makeText(MainActivity.this, "Registered Successfully",
                                    Toast.LENGTH_SHORT).show();
                            progressDialog.hide();

                            finish();

                            if(firebaseAuth.getCurrentUser() != null){
                                //profile activity
                                finish();
                                startActivity(new Intent(getApplicationContext(), WelcomeActivity2.class));
                            }
                        }
                        else{
                            //On Fail
                            Toast.makeText(MainActivity.this, "Register Failed.\nTry Again.",
                                    Toast.LENGTH_SHORT).show();
                            progressDialog.hide();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        if(v == btnSignUp){
            registerUser();
        }
        else if(v == textLogIn){
            //Open login activity
            finish();
           // startActivity(new Intent(this, LoginActivity.class));
        }

    }

    protected void onStop() {
        super.onStop();
        SharedPreferences sharedPreferences= getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("THE_DATE", String.valueOf(Calendar.getInstance().getTime()));
        editor.apply();
    }
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPreferences= getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("THE_DATE", String.valueOf(Calendar.getInstance().getTime()));
        editor.apply();
    }
    protected void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences= getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String  date= sharedPreferences.getString("Date", "");
        Toast.makeText(this, date, Toast.LENGTH_SHORT).show();
    }
    protected void onResume() {

        super.onResume();
        SharedPreferences sharedPreferences= getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String  date= sharedPreferences.getString("Date", "");
        Toast.makeText(this, date, Toast.LENGTH_SHORT).show();
    }
}

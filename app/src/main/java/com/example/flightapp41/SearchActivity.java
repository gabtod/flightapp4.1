package com.example.flightapp41;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class SearchActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences sharedPreferences= getSharedPreferences("user info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        EditText txtYear = findViewById(R.id.txtSearchYear);
        editor.putString("year", txtYear.getText().toString());

        editor.commit();
    }
    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences sharedPreferences= getSharedPreferences("user info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        EditText txtYear = findViewById(R.id.txtSearchYear);
        editor.putString("year", txtYear.getText().toString());
        editor.commit();
    }
    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences= getSharedPreferences("user info", Context.MODE_PRIVATE);
        String year = sharedPreferences.getString("year", "");
        EditText txtSearchYear = findViewById(R.id.txtSearchYear);
        txtSearchYear.setText(year);
    }
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences= getSharedPreferences("user info", Context.MODE_PRIVATE);
        String year = sharedPreferences.getString("year", "");
        EditText txtSearchYear = findViewById(R.id.txtSearchYear);
        txtSearchYear.setText(year);
    }


    String AllDetails = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_welcome) {
            // Handle the camera action
            finish();
            startActivity(new Intent(getApplicationContext(),WelcomeActivity2.class));

        } else if (id == R.id.nav_saved) {
            finish();
            startActivity(new Intent(getApplicationContext(),SavedFlightsActivity.class));

        } else if (id == R.id.nav_search) {

        }
        else if (id == R.id.nav_view) {
            finish();
            startActivity(new Intent(getApplicationContext(),ViewFlightsActivity.class));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void getFlight(View v){

        EditText txtSearchYear = findViewById(R.id.txtSearchYear);
        EditText txtSearchMonth = findViewById(R.id.txtSearchMonth);
        EditText txtSearchDay = findViewById(R.id.txtSearchDay);
        EditText txtSearchTime = findViewById(R.id.txtSearchTime);
        String year = txtSearchYear.getText().toString();
        String month = txtSearchMonth.getText().toString();
        String day = txtSearchDay.getText().toString();
        String time = txtSearchTime.getText().toString();

       // String[] info = {txtSearchYear.getText().toString(), txtSearchMonth.getText().toString(), txtSearchDay.getText().toString(), txtSearchTime.getText().toString()};
        ArrayList<Details> detailsArray = new ArrayList<>();

        GetFlightsAsync task = new GetFlightsAsync();

        try {
            //Details d = task.execute(info[0], info[1], info[2], info[3]).get();
            detailsArray = task.execute(year, month, day,time).get();
            TextView txtOutput = findViewById(R.id.txtOutput);

            /*for (int i=0; i<info.length;i++) {
            String departureAirport = d.getDepartureAirport();
            String flightId = d.getFlightId();
            String dateAndTime = d.getDateAndTime();
            AllDetails = "Flight Id"+ flightId+". From: "+departureAirport+ ". Arrives At:" +dateAndTime;
            txtOutput.append(AllDetails);}*/

            for (int i=0; i<detailsArray.size();i++) {
                String departureAirport = detailsArray.get(i).getDepartureAirport();
                String flightId = detailsArray.get(i).getFlightId();
                String dateAndTime = detailsArray.get(i).getDateAndTime();
                AllDetails += "Flight Id"+ flightId+"\nFrom: "+departureAirport+ "\n Arrives At:" +dateAndTime+"\n\n\n";
            }
            txtOutput.setText(AllDetails);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}

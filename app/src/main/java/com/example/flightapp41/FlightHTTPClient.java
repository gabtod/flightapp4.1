package com.example.flightapp41;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FlightHTTPClient {
    private static String BASE_URL = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/MLA/arr/";
    // https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/MLA/arr/2019/5/31/15?appId=87453b2f&appKey=+564c61f3d7303c89d40d10bdd360d96e&utc=false&numHours=1&maxFlights=5
    // https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/MLA/arr/2019/6/1/10?&appid=87453b2f&appKey=+564c61f3d7303c89d40d10bdd360d96e&utc=false&numHours=6&maxFlights=50


    private static String year;
    private static String month;
    private static String day;
    private static String hourOfDay;
    private static String APP_ID = "&appId=87453b2f";
    private static String APP_KEY = "?appKey=+564c61f3d7303c89d40d10bdd360d96e";
    private static String utc = "&utc=false";
    private static String numHours ="&numHours=6";
    private static String maxFlights = "&maxFlights=50";

    public String getFlightData(String getYear, String getMonth, String getDay, String getHourOfDay) {
        HttpsURLConnection con = null ;
        InputStream is = null;

        try {
            String url = BASE_URL + getYear+ "/"+getMonth+"/"+getDay+"/"+getHourOfDay+APP_KEY+APP_ID+utc+numHours+maxFlights;


            con = (HttpsURLConnection) ( new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(false);
            con.connect();

            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null )
                buffer.append(line + "\r\n");

            is.close();
            con.disconnect();

            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }

        return null;

    }
}





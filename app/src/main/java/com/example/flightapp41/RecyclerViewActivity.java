package com.example.flightapp41;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.android.gms.common.util.CrashUtils;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity {

    private static final String TAG = "RecyclerViewActivity";

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        Log.d(TAG, "onCreate: started.");
        initImageBitmaps();
    }
    private void initImageBitmaps() {
        Log.d(TAG, "initImageBitmaps: preparing bitmaps");

        mImageUrls.add("https://miamain.blob.core.windows.net/wp-uploads/wp-content/uploads/2018/05/video.png");
        mNames.add("Airport Malta");

        mImageUrls.add("https://t-ec.bstatic.com/images/hotel/max1024x768/162/162201543.jpg");
        mNames.add("Airport Luton  London");

        mImageUrls.add("https://www.tripsavvy.com/thmb/fedSD5DSO6_voXRDkhoAoKzH5KE=/960x0/filters:no_upscale():max_bytes(150000):strip_icc()/GettyImages-143685782-593523f83df78c08ab1fa4d5.jpg");
        mNames.add("Airport Heathrow London");

        initRecyclerView();

    }

    private void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: init recyclerView.");
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mNames, mImageUrls);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}
